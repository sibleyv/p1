> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Vincent Sibley

### Project 1: Client-side data validation

> Deliverables:
1. 1. Provide Bitbucket read-only access to p1 repo, *must* include README.md, using Markdown
syntax.

2. Blackboard Links: p1 Bitbucket repo


#### Project 1 Screenshots:

*Screenshots*:

*Failed validation*:
![Failed validation](img/fail.png)

*Passed validation*:
![Passed validation](img/pass.png)







